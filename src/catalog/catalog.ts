import * as fs from 'node:fs'
import * as path from 'node:path'
import * as readline from 'node:readline'

import type { BaseEntryType } from "./base_entry_type.ts"
import { BuildingType } from "./building_type.ts"
import { EntryTable } from "./entry_table.ts"
import { ModuleType } from "./module_type.ts"
import { PlatformType } from "./platform_type.ts"
import { ProcessType } from "./process_type.ts"
import { ResourceType } from "./resource_type.ts"
import { TagSet } from "./tag_set.ts"
import { TechnologyType } from "./technology_type.ts"
import { UnitType } from "./unit_type.ts"

const earlyExit = true

export class Catalog {
  building: EntryTable<BuildingType>
  module: EntryTable<ModuleType>
  platform: EntryTable<PlatformType>
  process: EntryTable<ProcessType>
  resource: EntryTable<ResourceType>
  technology: EntryTable<TechnologyType>
  unit: EntryTable<UnitType>
  tags: TagSet

  // if this catalog loaded correctly
  loaded: boolean
  verbosity: number

  constructor() {
    this.building = new EntryTable<BuildingType>()
    this.module = new EntryTable<ModuleType>()
    this.platform = new EntryTable<PlatformType>()
    this.process = new EntryTable<ProcessType>()
    this.resource = new EntryTable<ResourceType>()
    this.technology = new EntryTable<TechnologyType>()
    this.unit = new EntryTable<UnitType>()
    this.tags = new TagSet()
    this.loaded = false
    this.verbosity = 0
  }

  collectTags(table: EntryTable<BaseEntryType>): void {
    for (const entry of table.values()) {
      this.tags.merge((<BaseEntryType>entry).tags);
    }
  }

  collectAllTags(): void {
    this.collectTags(this.building)
    this.collectTags(this.module)
    this.collectTags(this.platform)
    this.collectTags(this.process)
    this.collectTags(this.resource)
    this.collectTags(this.technology);
    this.collectTags(this.unit);
  }

  #loadEntry<EntryType extends BaseEntryType>(line: string, archetype: EntryType, table: EntryTable<EntryType>): EntryType | null {

    try {
      // Unsafe Assignement -- Specific fields should still be checked for in the Type's constructor
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      const parsedEntry: EntryType = JSON.parse(line);

      let superEntry: EntryType
      let newEntry: EntryType;
      if ('super' in parsedEntry) {
        const superKey = <string>parsedEntry['super'];
        if (table.contains(superKey)) {
          superEntry = table.get(superKey)
          newEntry = <EntryType>superEntry.create(table.nextId, parsedEntry)
        } else {
          console.error(`!!!! Referenced non-existent super class while loading type: <${archetype.typeName}>  superKey=${superKey}`)
          return null;
        }
      } else {
        newEntry = <EntryType>archetype.create(table.nextId, parsedEntry)
      }

      return newEntry;
    } catch (err) {
      if(line.search(',}')){
        console.error("<<<!! Found extraneous comma before brace!")
      }
      console.error(err);
      return null;
    }
  }

  async #loadTable<EntryType extends BaseEntryType>(data_path: string, filename: string, archetype: EntryType): Promise<EntryTable<EntryType>> {

    const file_path = path.join(data_path, filename);
    const basename: string = filename.split(".")[0]
    if (0 <= this.verbosity)
      console.log(`    >>> Loading: all ${basename} entries`)

    const table = new EntryTable<EntryType>();

    let depth = 0
    let lineCount = 0; // line # in the json file, counting whitespace
    let entryCount = 0; // entry count, as processed

    const reader = readline.createInterface({ input: fs.createReadStream(file_path) });
    let buffer = '';

    for await (const line of reader) {
      ++lineCount;

      if (line.trimStart().startsWith('//')) {
        continue;
      }
      if (line.length < 1) {
        continue;
      }

      // // if (("platforms" == basename) && (45 < lineCount) && (lineCount < 58)) {
      //   console.log(`@[${lineCount.toString().padStart(3)}]: ${line}`);
      // // }

      const count_open_brace = (line.match(/{/g) || []).length
      const count_close_brace = (line.match(/}/g) || []).length
      const cur_depth = depth + count_open_brace - count_close_brace

      if( 0 == depth ){
        if( 0 == cur_depth ){
          // start (and finish) a simple-entry
          // console.log(`:[${lineCount.toString().padStart(3)}]:{}`)
          entryCount++
          buffer = line
        }else if( 0 < count_open_brace ){
          // start a multi-line-entry
          // console.log(`:[${lineCount.toString().padStart(3)}]:{`)
          buffer = line
        }else{
          console.log(`:[${lineCount.toString().padStart(3)}]:???`)
        }
      }else{
        if( 0 == cur_depth ){
          // finish a multi-line-entry
          // console.log(`:[${lineCount.toString().padStart(3)}]: }`)
          entryCount++
          buffer += line
        }else{
          // console.log(`:[${lineCount.toString().padStart(3)}]:    :`)
          buffer += line
        }
      }
      depth = cur_depth 

      // lets be permissive about these syntax options:
      buffer = buffer.trim() // remove extra whitespace
      buffer = buffer.replace(/,}/g,'}')  // allow extra commas on the last element
      buffer = buffer.replace(/},$/g,'}') // allow extra commas in between elements

      if((0 == depth) && (0 < buffer.length)) {
        const entry = this.#loadEntry(buffer, archetype, table);
        if (null === entry) {
          console.error(`  <<!! Invalid entry!! <${archetype.typeName}>:@${lineCount}`);
          console.error(`       :Line:  [${lineCount.toString().padStart(3)}]: ${line}`);
          console.error(`       :buffer:[${entryCount.toString().padStart(3)}]: ${buffer}`);

          if (earlyExit) {
            return table;
          }
        } else {
          table.add(entry);
          ++entryCount;
        }
      }
    }

    table.loaded = true

    if (0 <= this.verbosity)
      console.log(`        <<< Loaded: ${lineCount} lines; ${entryCount} entries.`);
    return table;
  }

  static async create(verbosity = 0): Promise<Catalog> {
    const catalog = new Catalog();
    catalog.verbosity = verbosity

    const data_path = 'data/catalog/'

    const building_file = 'buildings.json'
    const module_file = 'modules.json'
    const platform_file = 'platforms.json'
    const process_file = 'processes.json'
    const resource_file = 'resources.json'
    const technology_file = 'technologies.json';
    const unit_file = 'units.json'

    if (0 < verbosity)
      console.log(`>>> Loading data files from: ${data_path}`)

    catalog.building = await catalog.#loadTable<BuildingType>(data_path, building_file, new BuildingType())
    catalog.module = await catalog.#loadTable<ModuleType>(data_path, module_file, new ModuleType());
    catalog.platform = await catalog.#loadTable<PlatformType>(data_path, platform_file, new PlatformType())
    catalog.process = await catalog.#loadTable<ProcessType>(data_path, process_file, new ProcessType())
    catalog.resource = await catalog.#loadTable<ResourceType>(data_path, resource_file, new ResourceType())
    catalog.technology = await catalog.#loadTable<TechnologyType>(data_path, technology_file, new TechnologyType())
    catalog.unit = await catalog.#loadTable<UnitType>(data_path, unit_file, new UnitType())

    { // verify links -- i.e. that keys listed in one type refer to an actual entries listed other types

      // buildings
      catalog.building.valid = catalog.building.values().every(building => {
        return building.verify(catalog.module)
      })

      // modules
      catalog.module.valid = catalog.module.values().every(module => {
        return module.verify(catalog.process, catalog.resource)
      })

      // platforms
      catalog.platform.valid = catalog.platform.values().every(platform => {
        return platform.verify(catalog.module)
      })

      // resources have no links

      // technlogies
      catalog.technology.valid = catalog.technology.values().every(technology => {
        return technology.verify( catalog.technology )
      })

      // units
      catalog.unit.valid = catalog.unit.values().every( unit => {
        return unit.verify( catalog.module )
      })

    }

    { // debug
      // catalog.building.printAllEntries()
      // catalog.resource.printAllEntries();
      // catalog.resource.printMatchEntries( 'tiberium' );
      // catalog.resource.printMatchEntries( processes, 'tiberium');
      // catalog.platform.printAllEntries();
    } // debug

    catalog.loaded = (
      catalog.building.loaded && 
      catalog.module.loaded &&
      catalog.platform.loaded &&
      catalog.process.loaded &&
      catalog.resource.loaded &&
      catalog.technology.loaded &&
      catalog.unit.loaded
    );

    return catalog;
  }

}
