import type { BaseEntryType } from "./base_entry_type.ts"
import type { EntryTable } from "./entry_table.ts"
import type { ModuleType } from "./module_type.ts"
// import type { StorageType } from './storage_type.ts'
// import type { SensorType } from './sensor_type.ts'
import { TagSet } from './tag_set.ts'


export class UnitType implements BaseEntryType {
  readonly typeName: string = 'UnitType';

  // default values:
  readonly name: string = 'default-name';
  readonly desc: string = '';
  readonly index: number = 0;
  readonly key: Lowercase<string> = 'default_key'

  readonly platform: string = '<missing>'
  // map of module names => quantity
  readonly modules = new Map<string, number>()

  // all of these are synthetic values, calculated from the child platform & modules
  // readonly ammo = []
  // readonly sensor = SensorType.VISUAL;
  // readonly module: ???? 
  // readonly weapon = None
  readonly mass: number = 1000.0;  // units = kilograms
  readonly volume: number = 1.0;  // units == L (liters, litres)

  readonly superKey?: string;
  readonly tags: TagSet = new TagSet()

  create(entryIndex: number, loadEntry: UnitType): BaseEntryType {
    return new UnitType(entryIndex, this, loadEntry);
  }

  constructor(entryIndex = -1, superEntry?: UnitType, loadEntry?: UnitType) {
    this.index = entryIndex;

    if (superEntry) {
      this.desc = superEntry.desc;
      this.mass = superEntry.mass;
      this.volume = superEntry.volume;
      this.tags.merge(superEntry.tags);
    }

    if (loadEntry) {
      const moduleKeyCache = new Map<string, number>();

      for (const [key, value] of Object.entries(loadEntry)) {
        if ('key' === key) {
          this.key = <Lowercase<string>>value;
        } else if ('description'.startsWith(key)) {
          this.desc = <string>value;
        } else if ('index' === key) {
          console.error('!!!! not allowed to explicitly set the index field in input data. Aborting.');
          return;
        } else if ('modules'.startsWith(key)) {
          for (const [key, qty] of Object.entries(<Map<string, number>>value)) {
            moduleKeyCache.set(key, <number>qty);
          }
        } else if ('name' === key) {
          this.name = <string>value;

        } else if ('platform'.startsWith(key)) {
          this.platform = <string>value;

        } else if ('super' === key) {
          this.superKey = <string>value;
        } else if (key.startsWith('tag')) {
          this.tags.update(<Array<string>>value);
        } else {
          console.error(`!!!! Could not load JSON key: ${key} into class ${this.constructor.name}`);
          return;
        }
      }
    }
  }

  verify(modules: EntryTable<ModuleType>): boolean {
    let valid = true

    this.modules.forEach((_qty: number, key: string) => {
      if (!modules.contains(key)) {
        console.log(`    !! @<${this.typeName}>: ${this.key.padEnd(32)}  ... could not find module: ${key}`)
        valid = false
      }
    });

    return valid
  }

  toString(): string {
    let str = `          - [${this.index.toString().padStart(3)}][${this.key}]: "${this.name}"`;
    if (this.superKey) {
      str += `\n              :^:${this.superKey}`;
    }

    if (0 < this.modules.size) {
      str += '              :: Modules: ';
      this.modules.forEach((qty, mod) => {
        const pad = 20;
        str += `                  - ${mod.padEnd(pad, '.')}`
          + ` ${(0 < qty) ? ' ' : ''} ${qty}\n`;
      });
    }

    if (this.tags) {
      str += `\n              :[${this.tags.toString()}]`;
    }

    return str;
  }

  valid(): boolean {
    return true;
  }
}

