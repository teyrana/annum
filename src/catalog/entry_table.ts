import type { BaseEntryType } from './base_entry_type.ts'

export class EntryTable<EntryType extends BaseEntryType> {

  map: Map<string, EntryType>;
  index: EntryType[];
  loaded: boolean
  valid: boolean

  constructor() {
    this.map = new Map<string, EntryType>()
    this.index = []
    this.loaded = false
    this.valid = false
  }

  add(entry: EntryType): void {
    this.map.set(entry.key, entry);
    this.index[entry.index] = entry;
  }

  at(lookup: number): EntryType {
    return this.index[lookup];
  }

  get(lookup: string): EntryType {
    return this.map.get(lookup)!;
  }

  contains(lookup: string): boolean {
    return this.map.has(lookup);
  }

  get nextId() : number { 
    return this.index.length;
  }

  get size(): number {
    return this.index.length;
  }

  printAllEntries() {
    if( 0 < this.size ){
      console.log(`    ## Printing entries for: <${this.index[0].typeName}>:(all)(${this.size})`);
      for (const entry of this.map.values()) {
        console.log(entry.toString());
      }
    }
  }

  printMatchEntries(tag: string) {
    if( 0 < this.size ){
      console.log(`    ## Printing entries for: <${this.index[0].typeName}>  matching: ${tag}`);
      for (const entry of this.map.values()) {
        if (entry.tags.has(tag)) {
          console.log(entry.toString());
        }
      }
    }
  }

  values(): EntryType[] {
    return this.index
  }

}

