import type { BaseEntryType } from './base_entry_type.ts'
import { EntryTable } from './entry_table.ts';
import { TagSet } from './tag_set.ts'

export class TechnologyType implements BaseEntryType {
  readonly typeName: string = 'TechnologyType';

  // default values:
  readonly name: string = 'default-name';
  readonly desc: string = '';
  readonly index: number = 0;
  readonly key: Lowercase<string> = 'default_key';
  
  readonly requires = new Set<string>();

  readonly tags = new TagSet();

   create( entryIndex: number, loadEntry: TechnologyType ): BaseEntryType {
    return new TechnologyType( entryIndex, this, loadEntry );
  }

  constructor( entryIndex = -1, superEntry?: TechnologyType, loadEntry?: TechnologyType ){
    this.index = entryIndex;

    if( superEntry ){
      this.desc = superEntry.desc;
      this.tags.merge(superEntry.tags);
      superEntry.requires.forEach( ea => { this.requires.add(ea); });
    }

    if( loadEntry ){
      for( const [key,value] of Object.entries(loadEntry)){
        if('key' === key){
          if( typeof value == 'string' ){
            this.key = <Lowercase<string>>value.toString().toLowerCase();
          }else{
            this.key = '';
          }
        }else if('desc' === key){
          this.desc = <string>value;
        }else if('index' === key){
          console.error('!!!! not allowed to explicitly set the index field in input data. Aborting.');
          return;
        }else if('name' === key){
          this.name = loadEntry.name;
        }else if( ('req'.startsWith(key)) || ('req'.startsWith(key)) ){
          if( typeof value === 'string'){
            this.requires.add(value);
          }else{
            (<Set<string>>value).forEach( ea => {
              this.requires.add(ea);
            });
          }
        }else if('description'.startsWith(key)){
          this.desc = loadEntry.desc;
        }else if(key.startsWith('tag')){
          this.tags.update( <Array<string>>value );
        }else{
          console.error(`!!!! Could not load JSON key: ${key} into class ${this.constructor.name}`);
        }
      }
    }
  }

  verify( techs: EntryTable<TechnologyType> ) : boolean {
   let valid = true

    this.requires.forEach( req => {
      if( ! techs.contains(req) ){
        console.log(`    !! could not find 'prerequisite' tech: ${req} from tech: ${this.key}`)
        valid = false
      }
    });

    return valid
  }

  toString(): string {
    let str = '';
    str += `          - [${this.index.toString().padStart(3)}][${this.key}]: "${this.name}"\n`;

    if( this.requires.size){
      str += '              :: Requires:\n';
      str += `                  - ${Array.from(this.requires).join(", ")}\n`;
    }

    if( 0 < this.tags.size){
      str += `              :: Tags: [${this.tags.toString()}]\n`;
    }
    return str;
  }

  valid(): boolean {
    return true;
  }
}

