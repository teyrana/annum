
export class TagSet extends Set<string> {

  update( other: Array<string> ): void {
    other.forEach( (tag: string) => { this.add(tag); } );
  }

  merge( other: TagSet ): void {
    other.forEach( (tag: string) => { this.add(tag); } );
  }

  toString(): string {
    return Array.from(this).join(',');
  }

}
