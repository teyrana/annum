import type { TagSet } from "./tag_set.ts"

export interface BaseEntryType {
  readonly typeName: string;

  readonly desc: string;
  readonly name: string;
  readonly index: number;
  readonly key: Lowercase<string>;
  readonly tags: TagSet;

  // primary constructor for creating new entries
  create( entryIndex: number, loadEntry: BaseEntryType ): BaseEntryType;
  
  // constructor( entryIndex = -1,
  //              superEntry?: <EntryType>,
  //              loadEntry?: <EntryType> );

  toString(): string;

}
