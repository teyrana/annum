export class MountType {

  readonly name: string;
  
  private readonly _code: number;

  private static _lookup = new Map<number,MountType>();

  private static _nextId = 0

  constructor( name: string ){
    this.name = name
    this._code = MountType._nextId
    MountType._nextId++
    MountType._lookup.set( this._code, this);
  }

  // catch-all -- mounts which don"t have any managed mounting mechanics or limitations
  static readonly INTERNAL = new MountType("INTERNAL")

  // hand-carried -- must be carried by human / infantry units
  static readonly HAND =     new MountType("HAND");

  // permanent structure
  static readonly STATIONARY =     new MountType("STATIONARY");

  // fixed-aspect mount, bolted down to a vehicle
  static readonly FIXED =     new MountType("FIXED")


  get code(): string {
    return String.fromCharCode(this._code);
  }

  static parse( value: number|string ): MountType {
    if( typeof value === "number") {
      const code = <number>value
      if( this._lookup.has(code) ){
        return this._lookup.get(code)!
      }
    }else if( typeof value === "string" ){
      const text = (<string>value).toLowerCase()
      if( "hand" == text ){
        return MountType.HAND
      }else if( text.startsWith("int") ){
        return MountType.INTERNAL
      }else if( text.startsWith("stat") ){
        return MountType.STATIONARY
      }
    }
   
    return MountType.INTERNAL;
  }

  toString() : string { 
    return this.name 
  }

}



