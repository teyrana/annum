

export class Footprint {
  readonly x: number
  readonly y: number

  constructor(x: number, y: number) {
    this.x = x
    this.y = y
  }

  get height(): number {
    return this.y
  }
  get width(): number {
    return this.x
  }

  static create_from_named(named: { height: number, width: number })
    : Footprint {
    return new Footprint(named.height, named.width);
  }

  static create_from_array(array: number[]): Footprint {
    return new Footprint(array[0], array[1])
  }

  copy(): Footprint {
    return new Footprint(this.x, this.y)
  }

  equals(other: Footprint): boolean {
    return ((this.x == other.x) && (this.y == other.y))
  }

  toString(): string {
    return `[ ${this.x}, ${this.y} ]`
  }

}

