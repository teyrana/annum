import type { BaseEntryType } from "./base_entry_type.ts"
import { Footprint } from "./footprint.ts"
import { EntryTable } from "./entry_table.ts"
import type { ModuleType } from "./module_type.ts"
import { MountType } from "./mount_type.ts";
// import { SensorType } from './sensor_type.ts'
import { TagSet } from "./tag_set.ts"


export class BuildingType implements BaseEntryType {
  readonly typeName: string = 'BuildingType';

  readonly name: string = 'default-name';
  readonly desc: string = '';
  readonly index: number = 0;
  readonly key: Lowercase<string> = 'default_key';

  readonly armor: number = 10.0;

  // this 10x10x10 === 1 map tile is the default building size
  readonly footprint = new Footprint( 10, 10 )

  readonly hitpoints: number = 1000;

  readonly volume: number = 0  // available volume for internal modules
  readonly mounts: MountType[] = [] // available external module mounts
  readonly modules = new Set<string>

  readonly superKey?: string;
  readonly tags = new TagSet();

  create(entryIndex: number, loadEntry: BuildingType): BaseEntryType {
    return new BuildingType(entryIndex, this, loadEntry);
  }

  constructor(entryIndex = -1, superEntry?: BuildingType, loadEntry?: BuildingType) {
    this.index = entryIndex;
    this.mounts.push( MountType.INTERNAL )

    if (superEntry) {
      this.desc = superEntry.desc;
      this.armor = superEntry.armor;
      this.footprint = superEntry.footprint.copy()
      this.hitpoints = superEntry.hitpoints;
      this.tags.merge(superEntry.tags);
    }

    if (loadEntry) {
      for (const [key, value] of Object.entries(loadEntry)) {
        if ('armor' === key) {
          this.armor = <number>value;
        } else if ('desc'.startsWith(key)) {
          this.desc = <string>value;
        } else if (("footprint".startsWith(key)) || ("size"==key)){
          if( 2 === value.length ){
            this.footprint = Footprint.create_from_array(value)
          }
        } else if (('hp' === key) || ('hitpoints' === key)) {
          this.hitpoints = <number>value;
        } else if ('index' === key) {
          console.error('!!!! not allowed to explicitly set the index field in input data. Aborting.');
          return;
        } else if ('key' === key) {
          this.key = <Lowercase<string>>(<string>value).toLocaleLowerCase();
        }else if('name' === key){
          this.name = <string>value;
        } else if (key.startsWith('module')) {
          if (typeof value === 'string') {
            this.modules.add(value);
          } else {
            (<Set<string>>value).forEach(m => { this.modules.add(m); });
          }
        }else if ( "mount" === key ){
          if (typeof value === 'string') {
            this.mounts.push( MountType.parse(value))
          }else if( typeof value == 'object' ){
            value.map( (v:string) => MountType.parse(v) ).forEach( (m:MountType) => this.mounts.push(m) );
          }
        } else if ('super' === key) {
          this.superKey = <string>value;
        } else if (key.startsWith('tag')) {
          this.tags.update(<Array<string>>value);
        } else if (key === 'volume') {
          this.volume = <number>value;

        }else{
          console.error(`!!!! Could not load into: ${this.constructor.name} // at entry: ${loadEntry.key}    // json key: ${key}`);
        }
      }

    }

  }

  verify(modules: EntryTable<ModuleType>): boolean {
    let valid = true

    this.modules.forEach((key: string) => {
      if (!modules.contains(key)) {
        console.log(`    !! could not find module: ${key} in platform: ${this.key}`);
        valid = false
      }
    });

    return valid
  }

  toString(): string {
    let str = '';
    str += `          - [${this.index.toString().padStart(3)}][${this.key}]: "${this.name}"`;

    if (0 < this.modules.size) {
      str += '\n              :mods: '
        + Array.from(this.modules).join(', ');
    }

    if (0 < this.tags.size) {
      str += `              :: Tags: [${this.tags.toString()}]\n`;
    }
    return str;
  }

  valid(): boolean {
    return (null !== this.modules);
  }
}
