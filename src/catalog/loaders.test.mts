import { assert, assertAlmostEquals, assertEquals, assertFalse } from "https://deno.land/std@0.181.0/testing/asserts.ts";

// project imports
import { Catalog } from './catalog.ts'
import { MobilityType } from "./mobility_type.ts";
import { MountType } from "./mount_type.ts"
import { StorageType } from "./storage_type.ts";


// https://deno.land/manual@v1.32.1/basics/testing


// Compact form: name and function
Deno.test("Can load data files correctly ", async () => {

  // load all the data files once, and then validate that it proceeded correctly
  const catalog = await Catalog.create(-1);
  assert(catalog.loaded, "Catalog could not load entries!?")

  { // Load Modules
    assertEquals(26, catalog.building.size, "Not enough buildings loaded")

    // not exhaustive; just some spot checks
    {
      const storage = catalog.building.get("bulk_storage_1x1")
      assert(storage)
      assertEquals(storage.name, "Small Storage Shed (1x1)")
      assertAlmostEquals(storage.hitpoints, 10000, 1.0)
      assertAlmostEquals(storage.armor, 100, 1.0)
      assertEquals(10, storage.footprint.x)
      assertEquals(10, storage.footprint.y)
      assertAlmostEquals(storage.volume, 100, 1.0)
      assertEquals(1, storage.mounts.length)
      assertEquals(MountType.INTERNAL, storage.mounts.at(0))
      assertEquals(1, storage.modules.size)
      assert(storage.modules.has("bulk_storage"))
    }
  } { // Load Modules
    assertEquals(145, catalog.module.size, "Not enough modules loaded")

    // not exhaustive; just some spot checks
    {
      const bulk = catalog.module.get('bulk_storage')
      assert(bulk)
      assertEquals('Bulk Storage', bulk.name)
      assertEquals(0, bulk.tags.size)
      assertFalse(bulk.superKey)
      assertAlmostEquals(10000, 1.0, bulk.mass)
      assertAlmostEquals(1000, 1.0, bulk.volume)
      assert(bulk.store.has("bulk"))
    } {
      const o2gen = catalog.module.get('o2_generator')
      assert(o2gen)
      assertEquals('O2 Generator', o2gen.name)
      assert(o2gen.tags.has('habitation'))
      assert(o2gen.tags.has('life_support'))
      assertFalse(o2gen.superKey)
      assertAlmostEquals(250, 1.0, o2gen.volume)
    }
  } { // Load Platforms
    assertEquals(32, catalog.platform.size, "Not enough platforms loaded")

    // not exhaustive; just some spot checks
    {
      const human = catalog.platform.get('human')
      assert(human)
      assertEquals('Human', human.name)
      assertFalse(human.superKey)
      assertEquals(0, human.tags.size)
      assertAlmostEquals(1000, human.mass, 1.0)
      assertAlmostEquals(0, human.volume, 1.0)
      assertEquals(1, human.mounts.length)
      assertEquals(MountType.HAND, human.mounts.at(0))
      assertEquals(MobilityType.INFANTRY, human.mobility)
    } {
      const sedan = catalog.platform.get('commercial_auto')
      assert(sedan)
      assertEquals("Sedan", sedan.name)
      assertEquals("4wheel", sedan.superKey)
      assertEquals(0, sedan.tags.size)
      assertAlmostEquals(500, sedan.mass, 1.0)
      assertAlmostEquals(120, sedan.volume, 1.0)
      assertAlmostEquals(10, sedan.armor, 1.0)
      assertEquals(1, sedan.mounts.length)
      assertEquals(MountType.INTERNAL, sedan.mounts.at(0))
      assertEquals(MobilityType.WHEELED, sedan.mobility)
    }
  } { // Load Processes
    assertEquals(100, catalog.process.size)

    // not exhaustive; just some spot checks
    {
      const burn = catalog.process.get('burn_biomass_1mw')
      assert(burn)
      assertEquals('Burn Biomass', burn.name)
      assert(burn.tags.has('combustion'))
      assertFalse(burn.superKey)
      assert(burn.io.has("biomass"))
      assert(burn.io.has("co2"))
      assert(burn.io.has("oxygen"))
      assert(burn.io.has("heat"))
    }
  } { // Load Resources
    assertEquals(219, catalog.resource.size)

    // not exhaustive; just some spot checks
    {
      const oxy = catalog.resource.get('oxygen')
      assert(oxy)
      assertEquals('Oxygen', oxy.name)
      assert(oxy.tags.has('air'))
      assertEquals('gas', oxy.superKey)
      assertEquals(StorageType.GAS, oxy.store)
    } {
      const sheep = catalog.resource.get('sheep')
      assert(sheep)
      assertEquals('Live Sheep', sheep.name)
      assertAlmostEquals(400, sheep.volume, 1.0)
      assertAlmostEquals(160, sheep.mass, 1.0)
      assertEquals('livestock', sheep.superKey)
    } {
      const bl_tib = catalog.resource.get('blue_tiberium')
      assert(bl_tib)
      assertEquals('Blue Tiberium', bl_tib.name)
      assertAlmostEquals(0.5, bl_tib.volume, 1.0)
      assertAlmostEquals(1000, bl_tib.mass, 1.0)
      assertEquals(StorageType.DISCRETE, bl_tib.store) // should be HAZMAT?
      assertEquals('tiberium', bl_tib.superKey)
    }
  } { // Load Technologies
    assertEquals(51, catalog.technology.size)

    // not exhaustive; just some spot checks
    {
      const castIron = catalog.technology.get('cast_iron')
      assert(castIron)
      assertEquals("Cast Iron", castIron.name)
      assert(castIron.tags.has('steel'))
      assert(castIron.requires.has("smelt_iron"))
    }
  } { // Load Resources
    assertEquals(50, catalog.unit.size)
    {
      const conscript = catalog.unit.get('conscript')
      assert(conscript)
      assertEquals('Conscript Soldier', conscript.name)
      assertEquals(0, conscript.tags.size)
      assertEquals("person", conscript.superKey)
      assertEquals(0, conscript.modules.size)
    }
  }

});
