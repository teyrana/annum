import type { BaseEntryType } from './base_entry_type.ts'
// import type { EntryTable } from './entry_table.ts'
// import type { ResourceType } from './resource_type.ts'
import { TagSet } from './tag_set.ts'


export class ProcessType implements BaseEntryType {
  readonly typeName: string = 'ProcessType';

  // default values:
  readonly name: string = 'default-name';
  readonly desc: string = '';
  readonly index: number = 0;
  readonly key: Lowercase<string> = 'default_key';

  readonly io: Map<string, number> = new Map();

  readonly superKey?: string;
  readonly tags: TagSet = new TagSet();

  create( entryIndex: number, loadEntry: ProcessType ): BaseEntryType {
    return new ProcessType( entryIndex, this, loadEntry );
  }

  constructor( entryIndex = -1, superEntry?: ProcessType, loadEntry?: ProcessType) {
    this.index = entryIndex;

    if (superEntry) {
      this.name = 'placeholder'
      this.desc = superEntry.desc
      this.tags.merge(superEntry.tags)
    }

    if (loadEntry) {
      for (const [key, value] of Object.entries(loadEntry)) {
        if ('key' === key) {
          if (typeof value == 'string') {
            this.key = <Lowercase<string>>value.toString().toLowerCase();
          } else {
            this.key = '';
          }
        } else if ('name' === key) {
          this.name = <string>value;
        } else if ('io' === key) {
          if (typeof value === "object") {
            for (const [rsckey, qty] of Object.entries(<Map<string, number>>value)) {
              this.io.set(rsckey, <number>qty);
            }
          }
        } else if ('description'.startsWith(key)) {
          this.desc = loadEntry.desc;
        } else if (key.startsWith('tag')) {
          this.tags.update(<Array<string>>value);
        } else {
          console.error(`!!!! Could not load JSON key: ${key} into class ${this.constructor.name}`);
        }
      }
    }
  }

  // link( resources : EntryTable<ResourceType> ): boolean {
  //   // Load Dependent classes:
  //   // (A) I/O: find resources by key => load iff found
  //   this.io.forEach( (quantity: number, resourceKey:string) => {
  //       // console.log(`     @[${resourceKey}]: ${quantity}`);
  //       if( resources.contains(resourceKey) ){
  //           this.io.set( resources.get(resourceKey), quantity );
  //       }else{
  //           console.log(`    !! could not find resource: '${resourceKey}' for process: '${this.key}'`);
  //           return false;
  //       }
  //   });
  //   return true;
  // }

  toString(): string { 
    let str = '';
    str += `          - [${this.index.toString().padStart(3)}][${this.key}]: "${this.name}"\n`;

    if( 0 < this.io.size){
      str += `              :: Inputs / Outputs:\n`;
      this.io.forEach( (qty, rsc) => {
        const pad = 20;
        str += `                  - ${rsc.padEnd(pad,'.')}`
             + ` ${(0<qty)?' ':''} ${qty}\n`;
      });
    }
    if( 0 < this.tags.size){
      str += `              :: Tags: [${this.tags.toString()}]\n`;
    }
    return str;
  }

  valid(): boolean {
    if( 0 == this.key.length ){
      return false;
    }else if(null === this.io){
      return false;
    }
    return true;
  }

}

