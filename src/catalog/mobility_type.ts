
export class MobilityType {

  readonly name: string;
  private readonly code: number;
  private static lookup = new Map<number,MobilityType>();

  constructor( c:string, n:string ){
    this.name = n;
    this.code = c.toLowerCase().charCodeAt(0);
  }

  static readonly UNKNOWN =    new MobilityType('?', 'Unknown')
  static readonly STATIC =     new MobilityType('S', 'Static')
  static readonly INFANTRY =   new MobilityType('I', 'Infantry')
  static readonly WHEELED =    new MobilityType('W', 'Wheeled Vehicles')
  static readonly TRACKED =    new MobilityType('T', 'Tank Treads')
  static readonly RAIL =       new MobilityType('R', 'Rail Train')
  static readonly MECH =       new MobilityType('M', 'Mechanized Walker') // similiar to 'infantry', but heavier
  static readonly HELICOPTER = new MobilityType('H', 'Helicopter')
  static readonly AIRPLANE =   new MobilityType('A', 'Airplane')
  static readonly BOAT =       new MobilityType('B', 'Riverine Boat')     // shallow draft, may land/dock at beaches
  static readonly SHIP =       new MobilityType('O', 'Ocean Ship')        // deep draft, requires port facilities
  static readonly SUBMARINE =  new MobilityType('U', 'Submarine')         // deep draft, even when surfaced

  static parse( text: string ): MobilityType {
    text = text.trim().toLowerCase();
    if( 1 == text.length ){
      const code = text.charCodeAt(0);
      if( MobilityType.lookup.has(code) ){
        return MobilityType.lookup.get(code)!;
      }
    }else if( text === 'static' ){
      return MobilityType.STATIC
    }else if( text === 'infantry' ){
      return MobilityType.INFANTRY;
    }else if( text === 'wheel' ){
      return MobilityType.WHEELED;
    }else if( text === 'track' ){
      return MobilityType.TRACKED;
    }else if( text === 'rail' ){
      return MobilityType.RAIL;
    }else if( text === 'mech' ){
      return MobilityType.MECH;
    }else if( text === 'helo' ){
      return MobilityType.HELICOPTER;
    }else if( text == 'airplane' ){
      return MobilityType.AIRPLANE;
    }else if( text === 'river' ){
      return MobilityType.BOAT;
    }else if( text === 'ship' ){
      return MobilityType.SHIP;
    }else if( text === 'sub' ){
      return MobilityType.SUBMARINE;
    }

    return MobilityType.UNKNOWN
  }

  toString() : string {
    return this.name
  }

}