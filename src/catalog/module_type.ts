import type { BaseEntryType } from "./base_entry_type.ts"
// import { Cost } from "./cost.ts"
// import { StorageType } from "./storage_type.ts"
import type { EntryTable } from "./entry_table.ts"
import { TagSet } from "./tag_set.ts"
import type { ProcessType } from './process_type.ts'
import type { ResourceType } from './resource_type.ts'

export class ModuleType implements BaseEntryType {
  readonly typeName: string = 'ModuleType';

  // default values:
  readonly name: string = 'default-name';
  readonly desc: string = '';
  readonly index: number = 0;
  readonly key: Lowercase<string> = 'default_key';

  // mass and storage of the module _itself_
  readonly mass: number = 1000.0;  // units = kilograms
  readonly volume: number = 1000.0;  // units == L (liters, litres)

  // list of resources this module can run:
  readonly process = new Set<string>
  readonly mount: string = 'internal'

  // really storage-type -> quantity
  readonly store = new Map<string, number>

  readonly superKey?: string
  readonly tags: TagSet = new TagSet()

  create( entryIndex:number, loadEntry: ModuleType ): BaseEntryType {
    return new ModuleType( entryIndex, this, loadEntry );
  }

  constructor( entryIndex = -1, superEntry?: ModuleType, loadEntry?: ModuleType ){
    this.index = entryIndex;

    if( superEntry ){
      this.mass = superEntry.mass;
      this.desc = superEntry.desc;
      this.process = new Set(superEntry.process)
      this.mount = superEntry.mount
      this.store = new Map<string,number>(superEntry.store)
      this.tags.merge(superEntry.tags);
    }

    if( loadEntry ){
      // this.storage
      // this.process
      for( const [key,value] of Object.entries(loadEntry)){
        //console.log(`      ${key} : ${value}`);

        if('descr'.startsWith(key)){
          this.desc = <string>value;
        }else if('description'.startsWith(key)){
          this.desc = <string>value;
        }else if('key' === key){
          this.key = <Lowercase<string>>(<string>value).toLocaleLowerCase();
        }else if('mass' === key){
          this.mass = <number>value;
        }else if('mount' === key){
          this.mount = <string>value;
        }else if('name' === key){
          this.name = <string>value;

        }else if('processes'.startsWith(key)){  
          (<Set<string>>value).forEach( ea => { this.process.add(ea); } );

        }else if(('store' === key) || ('storage' === key)){
          // this is a Map< resource, quantity >
          // - storage type is a property of a resource, and merely inherited by storage
          for( const [key,qty] of Object.entries(<Map<string,number>>value) ){
            this.store.set( key, <number>qty );
          }
        }else if('super' === key){
          this.superKey = <string>value;
        }else if(key.startsWith('tag')){
          if( value instanceof Array<string> ){
            this.tags.update(<Array<string>>value);
          }
        }else if('volume'.startsWith(key)){
          this.volume = <number>value;
        }else{
          console.error(`!!!! Could not load JSON key: ${key} into class ${this.constructor.name} : ${loadEntry.key}`);
          return;
        }
      }
    }
  }

  verify( processes: EntryTable<ProcessType>, resources: EntryTable<ResourceType> ){
    let valid = true
  

    // .1. module => process
    this.process.forEach( (key:string) => {
      if( ! processes.contains(key) ){
        console.log(`    !!!! @<${this.typeName}>: ${this.key.padEnd(32)}  ... could not find process: ${key}`)
        valid = false;
      }
    });

    // .2. storage types valid:
    this.store.forEach( (_quantity: number, key: string ) => {
      if( ! resources.contains(key) ){
        console.log(`    !!!! <${this.typeName}> @[${this.key.padEnd(32)}].storage: ${key}`)
        valid = false;
      }
    });

    return valid;
  }

  toString(): string { 
    let str = `          - [${this.index.toString().padStart(3)}][${this.key}]: "${this.name}"`;
    // if( this.superKey ){
    //   str += `\n              :^:${this.superKey}`;
    // }

    if( this.mass ){
      str += `\n              :mass: ${this.mass}`;
    }
    if( this.mount ){
      str += `\n              :mount: ${this.mount}`;
    }

    if( 0 < this.process.size ){
      str += '\n              :proc: '
        + Array.from(this.process).join(', ');
    }
    if( 0 < this.store.size ){
      str += '\n              :store: '
          + Array.from(this.store, ([store_t,qty]) => `${store_t}:${qty}`).join(', ');
    }
    
    if( 0 < this.tags.size ){
      str += `\n              :[${this.tags.toString()}]`;
    }

    return str;
  }

  valid(): boolean {
    return ( (null !== this.process) && (null !== this.store) );
  }

}
