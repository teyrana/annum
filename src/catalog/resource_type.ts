import type { BaseEntryType } from './base_entry_type.ts'
import type { Catalog } from './catalog.ts'
import { StorageType } from './storage_type.ts'
import { TagSet } from './tag_set.ts'
import { Units } from "./units.ts"


export class ResourceType implements BaseEntryType {
  readonly typeName: string = 'ResourceType';

  // default values:
  readonly name: string = 'default-name';
  readonly desc: string = '';
  readonly index: number = 0;
  readonly key: Lowercase<string> = 'default_key';

  readonly units: Units = Units.KG_MASS;
  readonly mass: number = 1000.0;  // units = kilograms
  readonly volume: number = 1.0;  // units == L (liters, litres)
  readonly store: StorageType = StorageType.DISCRETE;
  readonly superKey?: string;
  readonly tags: TagSet = new TagSet()

  create( nextIndex: number, loadEntry: BaseEntryType ): BaseEntryType {
    return new ResourceType( nextIndex, this, loadEntry )
  }

  constructor(entryIndex = -1, superEntry?: ResourceType, loadData?: BaseEntryType) {
    this.index = entryIndex;

    if (superEntry) {
      this.desc = superEntry.desc
      this.units = superEntry.units
      this.mass = superEntry.mass
      this.volume = superEntry.volume
      this.store = superEntry.store
      this.tags.merge(superEntry.tags)
    }

    if (loadData) {
      for (const [key, value] of Object.entries(loadData)) {
        if ('key' === key) {
          if (typeof value == 'string') {
            this.key = <Lowercase<string>>value.toString().toLowerCase();
          } else {
            this.key = '';
          }
        } else if ('desc' === key) {
          this.desc = <string>value;
        } else if ('name' === key) {
          this.name = <string>value;
        } else if ('units' === key) {
          this.units = Units.parseUnitCode(<string>value);
        } else if ('density' === key) {
          if ('units' in loadData) {
            this.units = Units.parseUnitCode(<string>loadData['units']);
          }
          if ('density' in loadData) {
            const density: number = <number>loadData['density'];
            if (Units.KG_MASS === this.units) {
              this.mass = 1000.0;
              this.volume = 1.0 / density;
            } else if (Units.L_VOLUME === this.units) {
              this.mass = density;
              this.volume = 1.0;
            }
          }
        } else if ('mass' === key) {
          this.mass = <number>value;
        } else if ('volume'.startsWith(key)) {
          this.volume = <number>value;
        } else if (key.startsWith('desc')) {
          this.desc = <string>value;
        } else if (('store' === key) || ('storage' === key)) {
          this.store = StorageType.parse(<string>value);
        } else if ('super' === key) {
          this.superKey = <string>value;
        } else if (key.startsWith('tag')) {
          this.tags.update(<Array<string>>value);
        } else {
          console.error(`!!!! Could not load JSON key: ${key} into class ${this.constructor.name}`);
          return;
        }
      }
    }

    // no linking needed
  }

  toString(): string {
    let str = `          - [${this.index.toString().padStart(3)}][${this.key}]: "${this.name}"`

    if (this.superKey) {
      str += `\n              :^:${this.superKey}`
    }

    str +=  `\n              :>:${this.store.name}`

    // DEBUG
    str +=  `\n              ::mass:    ${this.mass}`
    str +=  `\n              ::volume:  ${this.volume}`

    if (0 < this.tags.size) {
      str += `\n              :[${this.tags.toString()}]`
    }

    return str;
  }

  get description(): string {
    return this.desc;
  }

}
