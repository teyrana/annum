

export class BoundingBox {

  // x-dimension
  readonly length: number
  // y-dimension
  readonly width: number
  // z-dimension
  readonly height: number

  constructor(height: number, width: number, length: number) {
    this.height = height
    this.width = width
    this.length = length
  }

  static create_from_named(named: { height: number, width: number, length: number }): BoundingBox {
    return new BoundingBox(named.height, named.width, named.length);
  }

  static create_from_array(array: number[]): BoundingBox {
    return new BoundingBox(array[0], array[1], array[2]);
  }

  copy(): BoundingBox {
    return new BoundingBox(this.length, this.width, this.height)
  }

  equals(other: BoundingBox): boolean {
    return ((this.length == other.length) && (this.width == other.width) && (this.height == other.height))
  }

  toString(): string {
    return `[ ${this.length}, ${this.width}, ${this.height} ]`
  }
}

