import type { BaseEntryType } from "./base_entry_type.ts"
import { BoundingBox } from "./bounding_box.ts"
import { EntryTable } from './entry_table.ts'
import { MobilityType } from './mobility_type.ts'
import type { ModuleType } from './module_type.ts'
import { MountType } from "./mount_type.ts"
// import { SensorType } from './sensor_type.ts'
import { TagSet } from './tag_set.ts'


export class PlatformType implements BaseEntryType {
  readonly typeName: string = 'PlatformType';

  readonly name: string = 'default-name';
  readonly desc: string = '';
  readonly index: number = 0;
  readonly key: Lowercase<string> = 'default_key';

  readonly armor: number = 10.0;
  // this 10x10x10 === 1 map tile is the default building & vehicle size
  readonly dimensions = new BoundingBox( 10, 10, 10 )
  // needed for buildings -> should define a canonical tile size -> 10m x 10m?
  // default size is this 1x1 tile === (10m x 10m)

  readonly mobility: MobilityType = MobilityType.STATIC;

  readonly hitpoints: number = 1000;

  readonly mass: number = 1000; // mass of platform itself
  readonly volume: number = 0;  // available volume for internal modules
  readonly mounts: MountType[] = []; // available external module mounts
  readonly modules = new Set<string>

  readonly superKey?: string;
  readonly tags = new TagSet();

  create(entryIndex: number, loadEntry: PlatformType): BaseEntryType {
    return new PlatformType(entryIndex, this, loadEntry);
  }

  constructor(entryIndex = -1, superEntry?: PlatformType, loadEntry?: PlatformType) {
    this.index = entryIndex;

    if (superEntry) {
      this.desc = superEntry.desc;
      this.mass = superEntry.mass;
      this.volume = superEntry.volume;
      this.armor = superEntry.armor;
      this.dimensions = superEntry.dimensions.copy()
      this.hitpoints = superEntry.hitpoints;
      this.mass = superEntry.mass;
      this.mounts = Array.from(superEntry.mounts)
      this.mobility = superEntry.mobility;
      this.volume = superEntry.volume;
      this.tags.merge(superEntry.tags);
    }

    if (loadEntry) {

      let new_length = this.dimensions.length
      let new_width = this.dimensions.width
      let new_height = this.dimensions.height
      
      for (const [key, value] of Object.entries(loadEntry)) {
        if ('armor' === key) {
          this.armor = <number>value;
        } else if ('desc'.startsWith(key)) {
          this.desc = <string>value;
        }else if( key === 'height'){
          new_height = <number>value;
        } else if (('hp' === key) || ('hitpoints' === key)) {
          this.hitpoints = <number>value;
        } else if ('index' === key) {
          console.error('!!!! not allowed to explicitly set the index field in input data. Aborting.');
          return;
        } else if ('key' === key) {
          this.key = <Lowercase<string>>(<string>value).toLocaleLowerCase();
        }else if( key === 'length'){
          new_length = <number>value;
        }else if('name' === key){
          this.name = <string>value;
        } else if ('mass' === key) {
          this.mass = <number>value;
        } else if (key.startsWith('module')) {
          if (typeof value === 'string') {
            this.modules.add(value);
          } else {
            (<Set<string>>value).forEach(m => { this.modules.add(m); });
          }
        } else if (key === 'mount') {
          if (typeof value === 'string') {
            this.mounts.push( MountType.parse(value))
          }else if( typeof value == 'object' ){
            value.map( (v:string) => MountType.parse(v) ).forEach( (m:MountType) => this.mounts.push(m) );
          }
        } else if (('move' === key) || ('mobility'.startsWith(key))) {
          this.mobility = MobilityType.parse(<string>value);
        } else if ('size' === key) {
          this.dimensions = BoundingBox.create_from_array( value )
        } else if ('super' === key) {
          this.superKey = <string>value;
        } else if (key.startsWith('tag')) {
          this.tags.update(<Array<string>>value);
        } else if (key === 'volume') {
          this.volume = <number>value;
        }else if(key === 'width'){
          new_width = <number>value;
        }else{
          console.error(`!!!! Could not load into: ${this.constructor.name} // at entry: ${loadEntry.key}    // json key: ${key}`);
        }
      }

      this.dimensions = new BoundingBox( new_height, new_width, new_length )
    }

  }

  verify(modules: EntryTable<ModuleType>): boolean {
    let valid = true

    this.modules.forEach((key: string) => {
      if (!modules.contains(key)) {
        console.log(`    !! could not find module: ${key} in platform: ${this.key}`);
        valid = false
      }
    });

    return valid
  }

  toString(): string {
    let str = '';
    str += `          - [${this.index.toString().padStart(3)}][${this.key}]: "${this.name}"`;

    if (0 < this.modules.size) {
      str += '\n              :mods: '
        + Array.from(this.modules).join(', ');
    }

    // str += "\n              :box: " + this.dimensions

    if( 0 < this.modules.size ){
      str += "\n              :module: [...]"
    }

    str += "\n              :move: " + this.mobility

    if( 0 < this.mounts.length ){
      str += "\n              :mount: [" + this.mounts.join(",") + "]"
    }

    if (0 < this.tags.size) {
      str += `              :: Tags: [${this.tags}]\n`;
    }
    return str;
  }

  valid(): boolean {
    return (null !== this.modules);
  }
}
