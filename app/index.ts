import express from 'npm:express';
import type { Request, Response } from 'npm:express';

import path from 'node:path';

const app = express();
const port = 3000;

const html_dir = path.join( Deno.cwd(), 'html' );

// fallback
app.get('/', ( _req:Request, res: Response ) => {
    res.sendFile(path.join(html_dir, 'index.html'));
});

app.use( '/assets', express.static( 'assets' ));
app.use( '/css', express.static( 'css' ));
app.use( '/data', express.static( 'data' ));
app.use( '/script', express.static( 'dist' ));
app.use( '/html', express.static('html'));

// app.use( '/src', express.static( 'src' ));

// app.get('/favicon.io', ( _req:Request, _res:Response ) => {
//   res.sendFile(path.join( html_dir, '/assets/favicon.ico'));
// });

app.get('/build', (_req:Request, res:Response ) => {
  res.sendFile(path.join( html_dir, 'build.html'));
});

app.get('/play', (_req:Request, res:Response ) => {
  res.sendFile(path.join( html_dir, 'play.html'));
});

app.get('/gen/image', (_req:Request, res:Response ) => {
  res.sendFile(path.join( html_dir, 'image.html'));
});

app.get('/gen/terrain', (_req:Request, res:Response ) => {
  res.sendFile(path.join( html_dir, 'terrain.html'));
});

app.listen(port, () => {
  return console.log(`Express is listening at http://localhost:${port}`);
});
